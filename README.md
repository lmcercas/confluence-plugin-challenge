# Confluence Plugin Challenge #

## Purpose ##

The purpose of this repository is to serve as a base for a challenge we use as part of our recruitment process

## Prerequisites ##

Java JDK 8 (tested with Oracle 1.8_60)

[Atlassian Plugin SDK, tested with 5.1.10](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project/set-up-the-sdk-prerequisites-for-linux-or-mac)

## Challenge Requirements ##

### Core Requirement ###

The core requirement (and the only one necessary to complete for this challenge) is to write a macro that displays all
users that commented on the page this macro is inserted.

### Extensions ###

It is not necessary to attempt any of these extension requirements. If you do attempt them, they are not in order and 
you can pick and choose which ones you complete

1. Use a Wired Test to test the macro. [Wired Tests are a kind of integration test](https://developer.atlassian.com/docs/getting-started/writing-and-running-plugin-tests).
 The code in this repository contains a basic Wired Test in the `it.*` package.
1. Display the users as Confluence displays them, with a hover over to get more information. Hint: you shouldn't need to
 write any code to get this to work.
1. Functional tests using [PageObjects](https://developer.atlassian.com/jiradev/jira-platform/other/tutorial-writing-integration-tests-using-pageobjects)
 (link is for JIRA, but similar applies to Confluence) and WebDriver
1. Anything else you can think of. Be creative


## How to Submit ##

1. Create a private fork of this repository. 
1. Make your changes and push.
1. Create a pull request back to this repository adding `jonmort` and `rfrancoadaptavist` as reviewers.
1. If you don't hear anything for a few days, email `recruitment` at `adaptavist.com`, or the person who gave you this challenge.


## Plugin SDK ##

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
