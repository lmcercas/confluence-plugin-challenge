package ut.com.adaptavist.recruitment.confluence;

import org.junit.Test;
import com.adaptavist.recruitment.confluence.MyPluginComponent;
import com.adaptavist.recruitment.confluence.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest {
    @Test
    public void testMyName() {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent", component.getName());
    }
}
