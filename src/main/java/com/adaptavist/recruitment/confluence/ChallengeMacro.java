package com.adaptavist.recruitment.confluence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;



public class ChallengeMacro implements Macro {

	
	private final XhtmlContent xhtmlUtils;
	
	public ChallengeMacro(XhtmlContent xhtmlUtils) {
		this.xhtmlUtils = xhtmlUtils;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		//String body = conversionContext.getEntity().getBodyAsString();
		final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
		final List<Comment> comments = conversionContext.getEntity().getComments();
		
//		try {
//			xhtmlUtils.handleMacroDefinitions(body, conversionContext, new MacroDefinitionHandler() {
//				
//				@Override
//				public void handle(MacroDefinition macroDefinition) {
//					macros.add(macroDefinition);
//					
//				}
//			});
//		} catch (XhtmlException e) {
//			throw new MacroExecutionException();
//		}
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("<p>");
		
		if(!comments.isEmpty())
		{
            
            builder.append("<table width=\"50%\">");
            builder.append("<tr><th>Comment User Name</th></tr>");
            for (Comment coment : comments)
            {
                builder.append("<tr>");
                builder.append("<td>").append(coment.getCreator().getName()).append("</td>");
                builder.append("</tr>");
            }
            builder.append("</table>");
        }
        else
        {
            builder.append("You've done built yourself a macro! Nice work.");
        }
		builder.append("</p>");

		return builder.toString();
	}

	@Override
	public BodyType getBodyType() {
		
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		
		return OutputType.BLOCK;
	}

}
